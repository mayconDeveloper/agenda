<?php
require './Controllers/readController.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link   href="css/bootstrap.min.css" rel="stylesheet">
    <script src="js/bootstrap.min.js"></script>
</head>

<body>
<div class="container">

    <div class="span10 offset1">
        <div class="row">
            <h3>Contatos</h3>
        </div>

        <div class="form-horizontal" >
            <div class="control-group">
                <label class="control-label">Nome</label>
                <div class="controls">
                    <label class="checkbox">
                        <?php echo $data['nome'];?>
                    </label>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">E-mail</label>
                <div class="controls">
                    <label class="checkbox">
                        <?php echo $data['email'];?>
                    </label>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Telefone</label>
                <div class="controls">
                    <label class="checkbox">
                        <?php echo $data['telefone'];?>
                    </label>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Endereço</label>
                <div class="controls">
                    <label class="checkbox">
                        <?php echo $data['endereco'];?>
                    </label>
                </div>
            </div>
            <div class="form-actions">
                <a class="btn" href="index.php">Voltar</a>
            </div>
        </div>
    </div>

</div>
</body>
</html>