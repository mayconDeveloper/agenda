<?php

require_once 'DataBase.php';
if ( !empty($_POST)) {
    $nomeError = null;
    $emailError = null;
    $telefoneError = null;
    $enderecoError = null;

    // keep track post values
    $nome = $_POST['nome'];
    $email = $_POST['email'];
    $telefone = $_POST['telefone'];
    $endereco = $_POST['endereco'];

    // validate input
    $valid = true;
    if (empty($name)) {
        $nomeError = 'Digite o nome';
        $valid = false;
    }

    if (empty($email)) {
        $emailError = 'Digite o e-mail';
        $valid = false;
    } else if ( !filter_var($email,FILTER_VALIDATE_EMAIL) ) {
        $emailError = 'Por favor inserir um endere�o de e-mail v�lido!';
        $valid = false;
    }

    if (empty($telefone)) {
        $telefoneError = 'Por favor inserir um n�mero de telefone';
        $valid = false;
    }

    if (empty($endereco)) {
        $enderecoEror = 'Por favor insira um endere�o';
        $valid = false;
    }

    // insert data
    if ($valid) {
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "INSERT INTO agenda (nome,email,telefone,endereco) values(?, ?, ?)";
        $q = $pdo->prepare($sql);
        $q->execute(array($nome,$email,$telefone,$endereco));
        Database::disconnect();
        header("Location: index.php");
    }
}