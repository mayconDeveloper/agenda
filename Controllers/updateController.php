<?php
require_once 'DataBase.php';

$id = null;
if ( !empty($_GET['id'])) {
    $id = $_REQUEST['id'];
}

if ( null==$id ) {
    header("Location: index.php");
}

if ( !empty($_POST)) {
    // keep track validation errors
    $nomeError = null;
    $emailError = null;
    $telefoneError = null;
    $enderecoError = null;
    // keep track post values
    $nome = $_POST['name'];
    $email = $_POST['email'];
    $telefone = $_POST['telefone'];
    $endereco = $_POST['endereco'];

    // validate input
    $valid = true;
    if (empty($nome)) {
        $nomeError = 'Digite um nome';
        $valid = false;
    }

    if (empty($email)) {
        $emailError = 'Digite um e-mail';
        $valid = false;
    } else if ( !filter_var($email,FILTER_VALIDATE_EMAIL) ) {
        $emailError = 'Digite um e-mail v�lido';
        $valid = false;
    }

    if (empty($telefone)) {
        $telefoneError = 'Digite um n�mero de telefone';
        $valid = false;
    }

    if (empty($endereco)) {
        $enderecoError = 'Digite o endere�o';
        $valid = false;
    }

    // update data
    if ($valid) {
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "UPDATE agenda  set nome = ?, email = ?, telefone =?, endereco=? WHERE id = ?";
        $q = $pdo->prepare($sql);
        $q->execute(array($nome,$email,$telefone,$endereco,$id));
        Database::disconnect();
        header("Location: index.php");
    }
} else {
    $pdo = Database::connect();
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "SELECT * FROM agenda where id = ?";
    $q = $pdo->prepare($sql);
    $q->execute(array($id));
    $data = $q->fetch(PDO::FETCH_ASSOC);
    $nome = $data['nome'];
    $email = $data['email'];
    $telefone = $data['telefone'];
    $endereco = $data['endereco'];
    Database::disconnect();
}