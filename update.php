<?php
require './Controllers/updateController.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link   href="css/bootstrap.min.css" rel="stylesheet">
    <script src="js/bootstrap.min.js"></script>
</head>

<body>
<div class="container">

    <div class="span10 offset1">
        <div class="row">
            <h3>Atualizar Contato</h3>
        </div>

        <form class="form-horizontal" action="update.php?id=<?php echo $id?>" method="post">
            <div class="control-group <?php echo !empty($nomeError)?'error':'';?>">
                <label class="control-label">Nome</label>
                <div class="controls">
                    <input name="nome" type="text"  placeholder="Nome" value="<?php echo !empty($nome)?$nome:'';?>">
                    <?php if (!empty($nomeError)): ?>
                        <span class="help-inline"><?php echo $nomeError;?></span>
                    <?php endif; ?>
                </div>
            </div>
            <div class="control-group <?php echo !empty($emailError)?'error':'';?>">
                <label class="control-label">E-mail</label>
                <div class="controls">
                    <input name="email" type="text" placeholder="Email" value="<?php echo !empty($email)?$email:'';?>">
                    <?php if (!empty($emailError)): ?>
                        <span class="help-inline"><?php echo $emailError;?></span>
                    <?php endif;?>
                </div>
            </div>
            <div class="control-group <?php echo !empty($telefoneError)?'error':'';?>">
                <label class="control-label">Telefone</label>
                <div class="controls">
                    <input name="mobile" type="text"  placeholder="Telefone" value="<?php echo !empty($telefone)?$telefone:'';?>">
                    <?php if (!empty($telefoneError)): ?>
                        <span class="help-inline"><?php echo $telefoneError;?></span>
                    <?php endif;?>
                </div>
            </div>
            <div class="control-group <?php echo !empty($enderecoError)?'error':'';?>">
                <label class="control-label">Endereço</label>
                <div class="controls">
                    <input name="mobile" type="text"  placeholder="Telefone" value="<?php echo !empty($endereco)?$endereco:'';?>">
                    <?php if (!empty($enderecoError)): ?>
                        <span class="help-inline"><?php echo $enderecoError;?></span>
                    <?php endif;?>
                </div>
            </div>
            <div class="form-actions">
                <button type="submit" class="btn btn-success">Atualizar</button>
                <a class="btn" href="index.php">Voltar</a>
            </div>
        </form>
    </div>

</div>
</body>
</html>