<?php
class Database
{
    private static $Nome = 'Agenda' ;
    private static $Host = 'localhost' ;
    private static $Usuario = 'sa';
    private static $Senha = '123456';

    private static $connec  = null;

    public function __construct() {
        die('Init function is not allowed');
    }

    public static function connect()
    {
        // One connection through whole application
        if ( null == self::$connec )
        {
            try
            {
                self::$connec =  new PDO( "mysql:host=".self::$Host.";"."dbname=".self::$Nome, self::$Usuario, self::$Senha);
            }
            catch(PDOException $e)
            {
                die($e->getMessage());
            }
        }
        return self::$connec;
    }

    public static function disconnect()
    {
        self::$connec = null;
    }
}
?>